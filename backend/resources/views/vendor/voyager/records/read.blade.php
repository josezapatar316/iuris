@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<style>
.row>[class*=col-]{
    margin-bottom:0px;
}
.spaces{
    display: flex;
    align-items: stretch;
    justify-content: space-between;
}
hr{
    margin-top: 10px;
    margin-bottom: 10px;

}
.gray{
    background:#efefef;
    padding-top:1em;
    padding-bottom:1em;
}
.no-gray{
    padding-top:1em;
    padding-bottom:1em;
}
</style>
@endsection
@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }} &nbsp;

        

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->
                   <div class="row">
                    <div class="col-md-6">
                        <label for="" class="page-title" style=" text-transform:uppercase;">Datos del expediente</label><button data-toggle="modal" data-target="#delete_modal" class="btn btn-success"><span class="voyager-chat"></span> Dejar recado al cliente {{$dataTypeContent->client_name}}</button>
                        <table class="table">
                            
                            <tr title="Expediente">
                                <td style="text-align:right;font-size:25px" ><span class="icon voyager-folder"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->nomenclature}}</td>
                            </tr>
                            <tr title="Nombre del cliente">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-person"></span></td>
                                <td style="font-size:20px; text-transform:uppercase"> <img src="/storage/{{$dataTypeContent->client_avatar}}" width="40px" alt=""> {{$dataTypeContent->client_name}}</td>
                            </tr>
                            <tr title="Cedula del cliente">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-credit-card"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->client_dni}}</td>
                            </tr>
                            <tr title="Nombre de la empresa y Rif">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-credit-card"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->company_name}} {{$dataTypeContent->rif}}</td>
                            </tr>
                            <tr title="Direccion de habitacion">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-company"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->client_direccionhab}}</td>
                            </tr>
                            <tr title="Telefono del cliente">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-phone"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->client_phone}}</td>
                            </tr>
                            <tr title="Nombre del abogado">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-person"></span><span class="voyager-book"></span></td>
                                <td style="font-size:20px; text-transform:uppercase"><img src="/storage/{{$dataTypeContent->layer_avatar}}" width="40px" alt="">{{$dataTypeContent->layer_name}}</td>
                            </tr>
                            <tr title="Cedula Abogado">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-credit-card"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->layer_dni}}</td>
                            </tr>
                            <tr title="Objeto">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-company"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->objeto_name}}</td>
                            </tr>
                            <tr title="Status">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-check"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->status_name}}</td>
                            </tr>
                            <tr title="Entidad">
                                <td style="text-align:right;font-size:25px" ><span class="voyager-world"></span></td>
                                <td style="font-size:20px; text-transform:uppercase">{{$dataTypeContent->entities_name}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <label for=""class="page-title" style="padding-left:0px; text-transform:uppercase;"><span class="voyager-archive"></span> Ultima Fecha de Junta</label><button data-toggle="modal" data-target="#delete_modal4" class="btn btn-success"><span class="voyager-file "></span>Fecha de junta</button>
                                <div class="row">
                                    @if($Metty!=null)
                                    <div class="col-md-12">
                                        <p style=" text-transform:uppercase;" >{{($Metty->subject)}}</p>                                
                                    </div>
                                    <div class="col-md-6">
                                        <p style=" text-transform:uppercase;"><span class="voyager-calendar"></span> Empezo: {{($Metty->start_date)}}</p>                                
                                    </div>
                                    <div class="col-md-6">
                                        <p style=" text-transform:uppercase;"><span class="voyager-calendar"></span> Termino: {{($Metty->end_date)}}</p>                                
                                    </div>
                                    @else
                                    <div class="col-md-6">
                                        <p style=" text-transform:uppercase;">No se han hecho juntas</p>
                                    <div>
                                    @endIf
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for=""class="page-title" style="padding-left:0px; text-transform:uppercase;"><span class="voyager-archive"></span> Tareas por hacer</label><button data-toggle="modal" data-target="#delete_modal4" class="btn btn-success"><span class="voyager-file "></span>Crear tarea</button>
                                <div class="row">
                                    @if(count($Task)>0)
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Tarea</th>
                                                    <th>Comienza</th>
                                                    <th>Termina</th>
                                                    <th>Tarea hecha?</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($Task as $task)
                                                <tr>
                                                    <td>{{$task->subject}}</td>
                                                    <td>{{$task->start_date}}</td>
                                                    <td>{{$task->end_date}}</td>
                                                    <td>
                                                        <form action="/admin/metties/{{$task->id}}" method="POST">
                                                            {{ method_field("PUT") }}
                                                            {{ csrf_field() }}
                                                            <button class="btn btn-{{$task->if_done==1?'success':'danger'}}">{{$task->if_done==1?'Ya se hizo':'No se ha hecho'}}</button>
                                                            <input type="hidden" name="subject" value="{{$task->subject}}">
                                                            <input type="hidden" name="start_date" value="{{$task->start_date}}">
                                                            <input type="hidden" name="end_date" value="{{$task->end_date}}">
                                                            <input type="hidden" name="if_remember" value="{{$task->if_remember}}">
                                                            <input type="hidden" name="if_done" value="{{$task->if_done==1?0:1}}">
                                                            <input type="hidden" name="record_id" value="{{$id}}">

                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                    <div class="col-md-6">
                                        <p style=" text-transform:uppercase;">No se han hecho juntas</p>
                                    <div>
                                    @endIf
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <label for=""class="page-title" style="padding-left:0px; text-transform:uppercase;"> <span class="voyager-book"></span> Notas y observaciones</label><button data-toggle="modal" data-target="#delete_modal3" class="btn btn-success"><span class="voyager-file "></span> Notas y observaciones</button>
                                <div class="row">
                                    <table class="table">
                                        <thead>
                                        
                                            @foreach($Note as $note)
                                            <tr>
                                                <td style=" text-transform:uppercase;">{{$note->description}}</td>
                                                <td style=" text-transform:uppercase;"><span class="badge badge-{{$note->if_note==1?'success':'primary'}}">{{$note->if_note==1?'Nota':'Observaciones'}}</span></td>
                                                <td style=" text-transform:uppercase;">{{$note->create_at}}</td>
                                            </tr>
                                            @endforeach
                                            @if(count($Note)==0)
                                                <p style=" text-transform:uppercase;">No hay notas u observaciones</p>
                                            @endif
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <label for="" class="page-title" style="padding-left:0px; text-transform:uppercase;">Archivos</label><button data-toggle="modal" data-target="#delete_modal2" class="btn btn-success"><span class="voyager-file "></span> Subir Archivo</button>
                                @if(count($File)==0)
                                                <p style=" text-transform:uppercase;">No hay Archivos para este expediente</p>
                                @endif
                                @if(count($File)>0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td style=" text-transform:uppercase;">Nombre del archivo</td>
                                            <td style=" text-transform:uppercase;">Peso mb</td>
                                            <td style=" text-transform:uppercase;">Extension</td>
                                            <td style=" text-transform:uppercase;">Descargar 
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                        @foreach($File as $file)
                                        <tr>
                                            <td style=" text-transform:uppercase;">{{$file->name}}</td>
                                            <td style=" text-transform:uppercase;">{{$file->size}}</td>
                                            <td style=" text-transform:uppercase; font-size:20px">
                                            @if($file->extention=="jpg")
                                                <span class='voyager-photo' title="{{$file->extention}}"></span>
                                            @else
                                                @if($file->extention=="docx"||$file->extention=="doc")
                                                    <span class='voyager-file-text' title="{{$file->extention}}" style="color:blue"></span>
                                                @else
                                                    @if($file->extention=="xlsx"||$file->extention=="xls")
                                                        <span class='voyager-file-text' title="{{$file->extention}}" style="color:green"></span>
                                                    @else
                                                        {{$file->extention}}
                                                    @endif
                                                @endif

                                            @endif
                                            </td>
                                            <td style=" text-transform:uppercase;"><a download href="/storage{{$file->ruta}}">Descargar</a></td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-success fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-chat"></i>Observaciones tratadas entre {{$dataTypeContent->client_name}} y {{$dataTypeContent->layer_name}}</h4>
                </div>
                <div class="modal-body" >
                    <div class="row" style="height:40em; overflow-y:auto">
                        @php 
                            $cont=0;
                        @endphp
                        @foreach($Chat as $chat)
                        @php $cont++;@endphp
                        <div class="col-md-12 new-register {{$chat->write_id==$dataTypeContent->layer_id?'gray':'no-gray'}}">
                            <div class="row" style="display: flex;align-items: center;justify-content: center;height: 4em;">
                                <div class="col-md-1">
                                    <img src="/storage/{{$chat->write_id==$dataTypeContent->client_id?$dataTypeContent->client_avatar:$dataTypeContent->layer_avatar}}" width="40px" alt="">
                                </div>
                                <div class="col-md-11">
                                    <div class="row">
                                        <div class="col-md-12 spaces">
                                            <span>{{$chat->write_id==$dataTypeContent->client_id?$dataTypeContent->client_name:$dataTypeContent->layer_name}}</span>
                                            <span><span class="badge badge-success">{{$cont==1?'Ultimo mensaje':''}}</span> {{$chat->created_at}}</span>
                                        </div>
                                        <div class="col-md-12">
                                            <span>{{$chat->description}}</span>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        

                        
                    </div>
                    <div class="row" style="background:#dcdcdc">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <form role="form" class="form-edit-add" action="/admin/chats" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-10">
                                <textarea required name="description" class="form-control" id="description" cols="30" rows="2"></textarea>
                            </div>
                            <div class="col-md-2">
                                <button  class="btn btn-success btn-block" style="font-size: 1.5em;" ><i class="voyager-paper-plane"></i></button>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <input type="hidden" name="record_id" id="record_id" value="{{$id}}">
                            <input type="hidden" name="wirte_to" id="wirte_to" value="{{$dataTypeContent->layer_id}}">
                            <input type="hidden" name="received_to" id="received_to" value="{{$dataTypeContent->client_id}}">
                        </form>
                    </div>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <div class="modal modal-success fade" tabindex="-1" id="delete_modal2" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-chat"></i>Subir archivos</h4>
                </div>
                <div class="modal-body" >
                    
                    <div class="row" >
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <form action="/admin/records/files/{{$id}}/uploadImage" class="dropzone" method="post" enctype="multipart/form-data">@csrf
                            
                            </form>
                        </div>
                        <div class="col-md-12" style="display:flex;align-items: flex-end;justify-content: flex-end;">
                            <button class="btn btn-success" onclick="load()">Finalizar</button>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal modal-success fade" tabindex="-1" id="delete_modal3" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-chat"></i>Agregar Nota / Observaciones</h4>
                </div>
                <div class="modal-body" >
                    <form role="form" class="form-edit-add" action="/admin/notes" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="row" >
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <input required type="text" class="form-control" name="description" placeholder="Descripcion" value="">
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="if_note" class="toggleswitch"> Seleccione si es una nota.
                            </div>
                            <div class="col-md-12" style="display:flex;align-items: flex-end;justify-content: flex-end;">
                                <button class="btn btn-success" >Crear Nota / observacion</button>
                            </div>
                        </div>
                        <input type="hidden" name="record_id" id="record_id" value="{{$id}}">
                    </form>
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal modal-success fade" tabindex="-1" id="delete_modal4" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-chat"></i>Tareas / fecha de junta</h4>
                </div>
                <div class="modal-body" >
                    <form role="form" class="form-edit-add" action="/admin/metties" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="row" >
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <label class="control-label" for="name">Asunto</label>
                                <input required type="text" class="form-control" name="subject" placeholder="Asunto" value="">
                            </div>
                            <div class="col-md-12">
                                <label class="control-label" for="name">Empieza</label>
                                <input required type="datetime" class="form-control datepicker" name="start_date" value="">
                            </div>
                            <div class="col-md-12">
                                <label class="control-label" for="name">Termina</label>
                                <input required type="datetime" class="form-control datepicker" name="end_date" value="">
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="if_remember" class="toggleswitch">
                                <label class="control-label" for="name">selecciona si es una tarea, si no, es una fecha de junta</label>
                            </div>
                            
                            <div class="col-md-12" style="display:flex;align-items: flex-end;justify-content: flex-end;">
                                <button class="btn btn-success" >Guardar fecha de junta</button>
                            </div>
                        </div>
                        <input type="hidden" name="record_id" id="record_id" value="{{$id}}">
                    </form>
                    
                </div>
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    <script src="{{asset('js/dropzone.js') }}"></script>

    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        function load(){
            window.location.href="/admin/records/{{$id}}"
        }
        function edit(x){
            console.log(x,$("#if_done"+x).val());
            $.ajax({
                dataType: "json",
                method:'put',
                url:"/admin/metties/"+x,   
                data: {
                    "if_done":$("#if_done"+x).val(),
                    "_token":'{{ csrf_field() }}'},
                success:function(data){
                    console.log(data)
                    
                }
            });
        }
        function save(){
            $.ajax({
                dataType: "json",
                method:'post',
                url:"/admin/chats",   
                data: $(".form-edit-add").serialize(),
                success:function(data){
                    console.log(data)
                    
                }
            });         
        }
    </script>
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
