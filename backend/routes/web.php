<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::resource("client","ClientsController");
    
    // Route::get("records/files/:id","FilesController@index");
    Route::get('records/files/{id}', 'FilesController@index');
    Route::get('records/files/{id}/create', 'FilesController@create');
    Route::post('records/files/{id}/uploadImage', 'FilesController@uploadImage');

    Route::get('records/notes/{id}', 'NotesController@index');
    Route::get('records/notes/{id}/create', 'NotesController@create');

    Route::get('records/metties/{id}', 'MettiesController@index');
    Route::get('records/metties/{id}/create', 'MettiesController@create');
    // Route::post('records/notes/{id}', 'NotesController@store');
    // Route::post('records/notes/{id}/uploadImage', 'NotesController@uploadImage');
});
