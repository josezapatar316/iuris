<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::resource("chats","ChatsController")->middleware("cors");;
Route::post("data","CastsController@data");
// Route::post("chat","ChatsController@store");
Route::resource("services","ServicesController");
Route::resource("testimonials","TestimonialsController");
Route::resource("casts","CastsController");
Route::resource("us-about","UsAboutsController");
Route::resource("records","RecordsController");
