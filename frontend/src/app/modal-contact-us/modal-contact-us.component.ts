import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-contact-us',
  templateUrl: './modal-contact-us.component.html',
  styleUrls: ['./modal-contact-us.component.css']
})
export class ModalContactUsComponent implements OnInit {
  
  profile : any = {
                  logo:"iuris.jpeg",
                  profile:"modal"
            };

  page_banner : any = {
        title:"Iniciar Sesion",
        profile:"modal",
  };

  constructor() { }

  ngOnInit(): void {
  }

}
