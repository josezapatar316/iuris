import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  public baseurl: string;
  temp: string;
  casts: any;

  constructor(private __http: HttpClient,  private http: HttpClient) {
    this.baseurl = environment.url;
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      // 'Authorization': 'Bearer '+ (localStorage.getItem('access_token'))
    })
  }
  get_store(data): Observable<any> {
    this.temp = this.baseurl + "/data";
    // console.log("--------------data:", data);
    return this.__http.post( this.temp,data,this.httpOptions )
    .pipe( retry(1),
      catchError(this.errorHandl)
    );
  }
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;// Get client-side error
    } else {
      errorMessage = error; // Get server-side error
    }
    return throwError(errorMessage);
  }
}
