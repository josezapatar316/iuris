import { Component, OnInit } from '@angular/core';
import { RecordsService } from "../services/records.service";
import { ChatsService } from "../services/chats.service";
import { environment } from "../../environments/environment";
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard-client',
  templateUrl: './dashboard-client.component.html',
  styleUrls: ['./dashboard-client.component.scss']
})
export class DashboardClientComponent implements OnInit {
  url=environment.url;
  storage=environment.urlstorage
  profile : any = {
    logo:"iuris.jpeg",
    profile:"modal"
};

page_banner : any = {
title:"Expedientes",
profile:"modal",
};

page_info : any = {
  title:"Expediente",
  image:"pic3.jpg",
  description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam, pellentesque enim ac, faucibus tortor. Nulla odio nibh, cursus sit amet urna id, dignissim euismod augue."
};

member : any = {
image:"",
logo:"",
designation:"",
age:"29",
dob:"",
residence:"USA",
address:"88 SOME STREET, SOME TOW",
email:"EMAIL@EXAMPLE.COM",
phone:"+0123 123 456 789",
skype:"ALEX.SMITH",
whatsapp:"",
about_me:"",
intrests:"",
name:"",
study:"",
highes_degree:"",
profile:"modal"
};


services : any = [
    {
      title:"CAMERA WORK",
      description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
    },
    {
      title:"POST PRODUCTION",
      description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
    },
    {
      title:"CINEMATOGRAPHY",
      description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
    },
    {
      title:"INVITATIONS CARDS",
      description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
    },
];
fun_facts : any = [
    {
      number:"2325",
      title:"HAPPY CLIENTS",
    },
    {
      number:"7582",
      title:"WORKING HOURS",
    },
    {
      number:"12",
      title:"AWARDS WON",
    },
    {
      number:"2000",
      title:"PROJECTS COMPLETED",
    },
];
testimonials : any = [
    {
      client_name:"Steve Tylor",
      image:"pic1.jpg",
      designation:"photographer",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
    {
      client_name:"David Matin",
      image:"pic3.jpg",
      designation:"artist",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
    {
      client_name:"Monica Rodriguez",
      image:"pic2.jpg",
      designation:"modal",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
    {
      client_name:"Steve Tylor",
      image:"pic1.jpg",
      designation:"photographer",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
    {
      client_name:"David Matin",
      image:"pic3.jpg",
      designation:"artist",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
    {
      client_name:"Monica Rodriguez",
      image:"pic2.jpg",
      designation:"modal",
      description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
    },
];
our_clients : any = [
    {
      client_title:"",
      logo:"logo5.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo3.jpg",
      link:"javascript:void(0);",
    },
    {
     client_title:"",
      logo:"logo1.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo2.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo6.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo4.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo5.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo6.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo1.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo2.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo6.jpg",
      link:"javascript:void(0);",
    },
    {
      client_title:"",
      logo:"logo4.jpg",
      link:"javascript:void(0);",
    },
];
  message=""
  data
  record: any;
  file: any;
  show: boolean=false;
  urls:SafeResourceUrl;;
  
  constructor(
    private recordsService:RecordsService,
    public sanitizer: DomSanitizer,
    private chatsService:ChatsService
  ) { }

  ngOnInit(): void {
    this.recordsService.get_show(10).subscribe(resp=>{
      console.log(resp)
      this.record=resp.dataTypeContent
      this.record.layer_avatar=this.storage+this.record.layer_avatar
      this.record.client_avatar=this.storage+this.record.client_avatar
      this.data=resp.chat
      this.file=resp.file
    })
    this.onActivate("");

  }
  showDocument(x){
    this.show=true
    this.file.filter(resp=>{
      if(resp.id==x){
        this.urls=this.sanitizer.bypassSecurityTrustResourceUrl(this.storage+resp.ruta);  
      }
    })
  }
  sendMessage(){
    let data={
      write_id:this.record.client_id,
      received_id:this.record.layer_id,
      description:this.message,
      record_id:this.record.record_id,
      api:1
    }
    console.log(data);
    this.chatsService.get_store(data).subscribe()
    this.recordsService.get_show(10).subscribe(resp=>{
      console.log(resp)
      this.record=resp.dataTypeContent
      this.record.layer_avatar=this.storage+this.record.layer_avatar
      this.record.client_avatar=this.storage+this.record.client_avatar
      this.data=resp.chat
      this.file=resp.file
    })
    
    this.message=""
    this.onActivate("");

  }
  onActivate($event){
    var element = document.getElementById("allbox");
    setTimeout(function(){ 
      element.scrollTop = 999999
    }, 100);
    // alert();
    
  }
}
