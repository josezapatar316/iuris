import { Component, OnInit } from '@angular/core';
import { CastService } from "../services/cast.service";
import { environment } from '../../environments/environment'
declare  var jQuery:  any;

@Component({
  selector: 'app-modal-portfolio',
  templateUrl: './modal-portfolio.component.html',
  styleUrls: ['./modal-portfolio.component.css']
})
export class ModalPortfolioComponent implements OnInit {
  url=environment.url;
  storage=environment.urlstorage
  profile : any = {
                  logo:"iuris.jpeg",
                  profile:"modal"
            };

  page_banner : any = {
        title:"Portfolio",
        profile:"modal",
  };
  
  categories : any = [
                  {
                    title:"Design",
                    filter_title:"design",
                  },
                  {
                    title:"Photography",
                    filter_title:"photography",
                  },
                  {
                    title:"development",
                    filter_title:"Development",
                  },
              ];

  portfolios : any = [
                  
  
              ]
  team: any;
  load=false
  constructor(
    private castService:CastService
  ) { }

  ngOnInit(): void {
    let data={
      api:1
    }
    this.load=false
    if(this.castService.get_casts()!=null){
      this.load=true
      this.portfolios=this.castService.get_casts()

    }else{
      this.castService.post_usAbout(data).subscribe(resp=>{
        console.log(resp)
        this.load=true

        resp.filter(resp2=>{
          this.portfolios.push({
            title:resp2.name,
            category:resp2.activity,
            image:this.storage+resp2.images,
            src_image:this.storage+resp2.images,
            link:"javascript:vois(0)",
            filter_class:"photography",
          })
        })
        this.castService.set_casts(this.portfolios)
      });
    }
	  (function ($) {
      
      setTimeout(function(){
        var self = jQuery("#masonry, .masonry");
        if(jQuery('.card-container').length)
          {
          self.imagesLoaded(function () {
            self.masonry({
              gutterWidth: 15,
              isAnimated: true,
              itemSelector: ".card-container"
            });
          });
        }
			
        jQuery(".filters").on('click','li',function(e) {

          var filter = jQuery(this).attr("data-filter");
          self.masonryFilter({
            filter: function () {
              if (!filter) return true;
              return jQuery(this).hasClass(filter);
            }
          });
        });
      }, 500);  
        
        
    })(jQuery);
  
  }

}
