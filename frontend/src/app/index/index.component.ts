import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
declare  var jQuery:  any;
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IndexComponent implements OnInit {

  profile : any = {
    logo:"iuris.jpeg",
    profile:"modal"
};

member : any = {
  image:"",
  logo:"http://admin.appbacus.com/storage/users/October2020/Yc5aIn6TPBjzNOkFnTX7.png",
  designation:"CEO IURIS",
  age:"29",
  dob:"",
  residence:"",
  address:"",
  email:"",
  phone:"",
  skype:"",
  whatsapp:"",
  about_me:"",
  intrests:"",
  name:"Ruben Ruben",
  study:"",
  highes_degree:"",
  profile:"modal"
};


constructor() { }

ngOnInit(): void {
(function ($) {

/* setTimeout(function(){ */
jQuery("#kenburn").slippry({
transition: 'kenburns',
useCSS: false,
speed: 3000,
pause: 3000,
auto: true,
kenZoom: 105,
preload: 'visible',
autoHover: false
});
/* }, 500);   */


})(jQuery);

}

}
