import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndexComponent } from './index/index.component';
import { Header1Component } from './elements/header/header1/header1.component';
import { SocialBarComponent } from './elements/social-bar/social-bar.component';
import { LoadingComponent } from './elements/loading/loading.component';
import { UserCardComponent } from './elements/user-card/user-card.component';
import { CopywriteComponent } from './elements/copywrite/copywrite.component';


// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ModalAboutUsComponent } from './modal-about-us/modal-about-us.component';
import { OurClientComponent } from './elements/our-client/our-client.component'
import { TestimonialComponent } from './elements/testimonial/testimonial.component'
import { FunFactCounterComponent } from "./elements/fun-fact-counter/fun-fact-counter.component";
import { MyServicesComponent } from "./elements/my-services/my-services.component";
import { PageBannerComponent } from "./elements/banner/page-banner/page-banner.component";
import { ModalResumeComponent } from "./modal-resume/modal-resume.component";
import { IconBoxComponent } from "./elements/icon-box/icon-box.component";
import { TagsComponent } from "./elements/tags/tags.component";
import { TimelineComponent } from "./elements/timeline/timeline.component";
import { LineComponent } from "./elements/progress-bar/line/line.component";
import { CircleComponent } from "./elements/progress-bar/circle/circle.component";
import { ModalPortfolioComponent } from "./modal-portfolio/modal-portfolio.component";
import { ModalContactUsComponent } from "./modal-contact-us/modal-contact-us.component";
import { HttpClientModule } from '@angular/common/http';
import { DashboardClientComponent } from './dashboard-client/dashboard-client.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoadingComponent,
    Header1Component,
    SocialBarComponent,
    CopywriteComponent,
    UserCardComponent,
    OurClientComponent,
    ModalAboutUsComponent,
    TestimonialComponent,
    FunFactCounterComponent,
    MyServicesComponent,
    PageBannerComponent,
    ModalResumeComponent,
    IconBoxComponent,
    TagsComponent,
    TimelineComponent,
    LineComponent,
    CircleComponent,
    ModalPortfolioComponent,
    ModalContactUsComponent,
    DashboardClientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
