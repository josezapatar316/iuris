import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardClientComponent } from './dashboard-client/dashboard-client.component';
import { IndexComponent } from './index/index.component';
import { ModalAboutUsComponent} from './modal-about-us/modal-about-us.component'
import { ModalContactUsComponent } from './modal-contact-us/modal-contact-us.component';
import { ModalPortfolioComponent } from './modal-portfolio/modal-portfolio.component';
import { ModalResumeComponent } from './modal-resume/modal-resume.component';

const routes: Routes = [
  
  {path:"",component:IndexComponent},
  {path:"acerca-iuris", component:ModalAboutUsComponent},
  {path:"resume", component:ModalResumeComponent},
  {path:"equipo", component:ModalPortfolioComponent},
  {path:"cliente", component:ModalContactUsComponent},
  {path:"expedientes", component:DashboardClientComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
